### Application needed

This application runs on docker kubernetes, theres three dependecies need before running this app.

- [Docker Desktop](https://www.docker.com/products/docker-desktop)
  Note: Please use docker desktop , using minikube you might encouter an error.
- [Kubernetes on Docker](#)
- [Skaffold](https://skaffold.dev/)
  Note: Please use Skaffold for one-command application start.

After you install the 3 required application just run skaffold dev on root directory.

### Installation

1. Docker Should be running.
2. Start Kubernetes on docoker.
3. Make sure you install Skaffold globaly
4. Go to root directory of this app

- skaffold
  ```sh
  skaffold dev
  ```

#### Go to your [http://localhost:31111/](http://localhost:31111/)
