import Head from 'next/head'
import styles from '../styles/Home.module.scss'
import axios from 'axios'
import { useState } from 'react'

export default function Home({ userInfoList }) {
  const [newAddressTitle, setNewAddressTitle] = useState('')
  const [userinfoData, setUserinfoData] = useState({
    userInfo: defaultUserInfo,
    indexSelected: -1,
  })
  const [updatedList, setUpdatesList] = useState(null)

  const selectedList = () => {
    if (updatedList !== null) {
      return updatedList
    }
    return userInfoList
  }

  const hadleOnchange = (value, target, key) => {
    if (target === 'userInfo') {
      setUserinfoData({
        ...userinfoData,
        userInfo: {
          ...userinfoData.userInfo,
          [key]: value,
        },
      })
    } else if (target === 'shippingAddress') {
      setUserinfoData({
        ...userinfoData,
        userInfo: {
          ...userinfoData.userInfo,
          shippingAddress: {
            ...userinfoData.userInfo.shippingAddress,
            [key]: value,
          },
        },
      })
    } else {
      setUserinfoData({
        ...userinfoData,
        userInfo: {
          ...userinfoData.userInfo,
          billingAddress: {
            ...userinfoData.userInfo.billingAddress,
            [key]: value,
          },
        },
      })
    }
  }

  const handleUpdate = async () => {
    const selectedData = selectedList()[userinfoData.indexSelected]

    try {
      const res = await axios.post(
        `http://localhost:32222/api/address/update/${selectedData._id}`,
        {
          ...userinfoData.userInfo,
          shipping: {
            ...userinfoData.userInfo.shippingAddress,
          },
          billing: {
            ...userinfoData.userInfo.billingAddress,
          },
        }
      )

      if (res.status === 200) {
        const updateData = await axios.get('http://localhost:32222/api/address')
        setUpdatesList(updateData.data)
        alert('Update successful.')
      }
    } catch (err) {
      // console.log(err.message)
      alert(err.response.data.errors[0].message)
    }
  }

  const handleCreateNew = async () => {
    try {
      const res = await axios.post(`http://localhost:32222/api/address/`, {
        userInfoTitle: newAddressTitle,
      })
      // console.log('RES DATA', res.data)
      setUpdatesList([
        ...selectedList(),
        {
          ...defaultUserInfo,
          ...res.data,
        },
      ])
      setNewAddressTitle('')
      alert('New address created successfuly.')
    } catch (err) {
      alert(err.response.data.errors[0].message)
    }
  }

  const handleDelete = async (id) => {
    try {
      const res = await axios.delete(`http://localhost:32222/api/address/${id}`)

      console.log(res.data)
      setUserinfoData({
        ...userinfoData,
        indexSelected: -1,
      })

      setUserinfoData({
        userInfo: defaultUserInfo,
        indexSelected: -1,
      })
      setUpdatesList(res.data)
    } catch (err) {
      alert(err.response.data.errors[0].message)
    }
  }

  const handleViewClick = (i) => {
    const selectedData = selectedList()[i]
    setUserinfoData({
      userInfo: {
        lastName: selectedData?.lastName || '',
        firstName: selectedData?.firstName || '',
        shippingAddress: {
          province: selectedData?.shippingAddress?.province || '',
          city: selectedData?.shippingAddress?.city || '',
          barangay: selectedData?.shippingAddress?.barangay || '',
          street: selectedData?.shippingAddress?.street || '',
        },
        billingAddress: {
          province: selectedData?.billingAddress?.province || '',
          city: selectedData?.billingAddress?.city || '',
          barangay: selectedData?.billingAddress?.barangay || '',
          street: selectedData?.billingAddress?.street || '',
        },
      },
      indexSelected: i,
    })
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div className={styles.form_container}>
          <form className={styles.form}>
            <div>
              <div className={styles.form_group}>
                <label htmlFor="firstName">First Name</label>
                <input
                  onChange={(e) =>
                    hadleOnchange(e.target.value, 'userInfo', 'firstName')
                  }
                  id="firstName"
                  value={userinfoData.userInfo.firstName}
                />
              </div>
              <div className={styles.form_group}>
                <label htmlFor="lastName">Last Name</label>
                <input
                  onChange={(e) =>
                    hadleOnchange(e.target.value, 'userInfo', 'lastName')
                  }
                  id="lastName"
                  value={userinfoData.userInfo.lastName}
                />
              </div>
            </div>
            <div>
              <h5>Billing Information</h5>
              <div className={styles.form_group}>
                <label htmlFor="provice">Province</label>
                <input
                  onChange={(e) =>
                    hadleOnchange(e.target.value, 'billingAddress', 'province')
                  }
                  id="province"
                  value={userinfoData.userInfo.billingAddress.province}
                />
              </div>
              <div className={styles.form_group}>
                <label htmlFor="city">City</label>
                <input
                  onChange={(e) =>
                    hadleOnchange(e.target.value, 'billingAddress', 'city')
                  }
                  id="city"
                  value={userinfoData.userInfo.billingAddress.city}
                />
              </div>
              <div className={styles.form_group}>
                <label htmlFor="barangay">Barangay</label>
                <input
                  onChange={(e) =>
                    hadleOnchange(e.target.value, 'billingAddress', 'barangay')
                  }
                  id="barangay"
                  value={userinfoData.userInfo.billingAddress.barangay}
                />
              </div>
              <div className={styles.form_group}>
                <label htmlFor="street">Street</label>
                <input
                  onChange={(e) =>
                    hadleOnchange(e.target.value, 'billingAddress', 'street')
                  }
                  id="street"
                  value={userinfoData.userInfo.billingAddress.street}
                />
              </div>
            </div>

            <div>
              <h5>Shipping Information</h5>
              <div className={styles.form_group}>
                <label htmlFor="provice">Province</label>
                <input
                  onChange={(e) =>
                    hadleOnchange(e.target.value, 'shippingAddress', 'province')
                  }
                  id="province"
                  value={userinfoData.userInfo.shippingAddress.province}
                />
              </div>
              <div className={styles.form_group}>
                <label htmlFor="city">City</label>
                <input
                  onChange={(e) =>
                    hadleOnchange(e.target.value, 'shippingAddress', 'city')
                  }
                  id="city"
                  value={userinfoData.userInfo.shippingAddress.city}
                />
              </div>
              <div className={styles.form_group}>
                <label htmlFor="barangay">Barangay</label>
                <input
                  onChange={(e) =>
                    hadleOnchange(e.target.value, 'shippingAddress', 'barangay')
                  }
                  id="barangay"
                  value={userinfoData.userInfo.shippingAddress.barangay}
                />
              </div>
              <div className={styles.form_group}>
                <label htmlFor="street"> Street </label>
                <input
                  onChange={(e) =>
                    hadleOnchange(e.target.value, 'shippingAddress', 'street')
                  }
                  id="street"
                  value={userinfoData.userInfo.shippingAddress.street}
                />
              </div>
            </div>
          </form>
          {userinfoData.indexSelected !== -1 ? (
            <button className={styles.update_btn} onClick={handleUpdate}>
              Update
            </button>
          ) : (
            <></>
          )}
        </div>
        <div className={styles.adress_list_container}>
          <h3>Address List</h3>
          <div>
            <input
              onChange={(e) => setNewAddressTitle(e.target.value)}
              value={newAddressTitle}
              placeholder="Adress Title"
            />
            <button onClick={handleCreateNew}>Create New</button>
          </div>

          <ul>
            {selectedList().length <= 0 ? (
              <p>Nothing to display.</p>
            ) : (
              selectedList().map((data, i) => {
                return (
                  <li
                    style={{
                      border:
                        userinfoData.indexSelected === i
                          ? '1px solid #15C4FF'
                          : 'none',
                    }}
                  >
                    <p>{data.userInfoTitle}</p>
                    <div>
                      <button onClick={() => handleViewClick(i)}>View</button>
                      <button
                        onClick={() => {
                          handleDelete(data._id)
                        }}
                      >
                        DELETE
                      </button>
                    </div>
                  </li>
                )
              })
            )}
          </ul>
        </div>
      </main>

      <footer className={styles.footer}>
        <a target="_blank" rel="noopener noreferrer">
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}

Home.getInitialProps = async () => {
  const res = await axios.get('http://flexiserver-srv:3000/api/address/')

  return {
    userInfoList: res.data,
  }
}

const defaultUserInfo = {
  userInfoTitle: '',
  lastName: '',
  firstName: '',
  shippingAddress: {
    province: '',
    city: '',
    barangay: '',
    street: '',
  },
  billingAddress: {
    province: '',
    city: '',
    barangay: '',
    street: '',
  },
}
