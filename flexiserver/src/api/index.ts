import express from 'express'
import { addressCreateRouter } from './user-address/create.routes'
import { addressDeleteRouter } from './user-address/delete.routes'
import { addressShowAllRouter } from './user-address/show-all.routes'
import { addressShowRouter } from './user-address/show.routes'
import { addressUpdateRouter } from './user-address/update.routes'

const router = express.Router()

router.use('/address', addressUpdateRouter)
router.use('/address', addressShowAllRouter)
router.use('/address', addressShowRouter)
router.use('/address', addressDeleteRouter)
router.use('/address', addressCreateRouter)

export default router
