import express, { Request, Response } from 'express'
import { body, validationResult } from 'express-validator'
import { AddressBillingInfo } from '../../model/address-billing-info.model'
import { AddressShippingInfo } from '../../model/address-shipping-info.model'
import { UserInfo } from '../../model/user-info.model'
import { Address } from '../../utils/addresses'
const router = express.Router()

router.post(
  '/',
  [
    body('userInfoTitle')
      .isString()
      .notEmpty()
      .withMessage('User info title should be string and not empty.'),
    body('firstName')
      .optional()
      .isString()
      .notEmpty()
      .withMessage('First name should be string'),
    body('lastName')
      .optional()
      .isString()
      .notEmpty()
      .withMessage('First name should be string'),
    body('shipping')
      .optional()
      .custom((add) => Address.isAddress(add))
      .withMessage('Shipping must be complete.'),
    body('billing')
      .optional()
      .custom((add) => Address.isAddress(add))
      .withMessage('Billing must be complete.'),
  ],
  async (req: Request, res: Response) => {
    const errorsValidation = validationResult(req)

    if (!errorsValidation.isEmpty()) {
      return res.status(400).send({
        errors: [
          {
            message: 'Incomplete information',
            field: errorsValidation.array(),
          },
        ],
      })
    }

    const { userInfoTitle, firstName, lastName, shipping, billing } = req.body
    console.log('firstname here', firstName)

    const userInfoExist = await UserInfo.findOne({
      userInfoTitle,
    })

    if (userInfoExist) {
      return res.status(400).send({
        errors: [
          {
            message: 'Title already Exist',
          },
        ],
      })
    }

    const userInfo = UserInfo.build({
      userInfoTitle,
      firstName,
      lastName,
    })

    if (shipping) {
      const shippingInfo = new AddressShippingInfo({
        province: shipping.province,
        city: shipping.city,
        barangay: shipping.barangay,
        street: shipping.street,
      })

      await shippingInfo.save()

      userInfo.shippingAddress = shippingInfo
    }

    if (billing) {
      const billingInfo = new AddressBillingInfo({
        province: billing.province,
        city: billing.city,
        barangay: billing.barangay,
        street: billing.street,
      })

      await billingInfo.save()

      userInfo.billingAddress = billingInfo
    }

    await userInfo.save()

    res.status(201).send(userInfo)
  }
)

export { router as addressCreateRouter }
