import express, { Request, Response } from 'express'
import { UserInfo } from '../../model/user-info.model'
const router = express.Router()

router.delete('/:id', async (req: Request, res: Response) => {
  const { id } = req.params
  await UserInfo.findByIdAndDelete(id)
    .populate('billingAddress')
    .populate('shippingAddress')
    .exec()

  const userInfoList = await UserInfo.find()
    .populate('billingAddress')
    .populate('shippingAddress')
    .exec()

  res.send(userInfoList)
})

export { router as addressDeleteRouter }
