import express, { Request, Response } from 'express'
import { UserInfo } from '../../model/user-info.model'
const router = express.Router()

router.get('/', async (req: Request, res: Response) => {
  const userInfoList = await UserInfo.find()
    .populate('billingAddress')
    .populate('shippingAddress')
    .exec()

  res.send(userInfoList)
})

export { router as addressShowAllRouter }
