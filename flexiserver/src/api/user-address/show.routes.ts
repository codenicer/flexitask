import express, { Request, Response } from 'express'
import { UserInfo } from '../../model/user-info.model'
const router = express.Router()

router.get('/:id', async (req: Request, res: Response) => {
  const { id } = req.params
  const userInfo = await UserInfo.findById(id)
    .populate('billingAddress')
    .populate('shippingAddress')
    .exec()

  res.send({ userInfo: userInfo })
})

export { router as addressShowRouter }
