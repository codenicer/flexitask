import express, { Request, Response } from 'express'
import { body, validationResult } from 'express-validator'
import { AddressBillingInfo } from '../../model/address-billing-info.model'
import { AddressShippingInfo } from '../../model/address-shipping-info.model'
import { UserInfo } from '../../model/user-info.model'
import { Address } from '../../utils/addresses'
const router = express.Router()

router.post(
  '/update/:id',
  [
    body('userInfoTitle')
      .optional()
      .isString()
      .notEmpty()
      .withMessage('User info title should be string and not empty.'),
    body('firstName')
      .optional()
      .isString()
      .notEmpty()
      .withMessage('First name should be string'),
    body('lastName')
      .optional()
      .isString()
      .notEmpty()
      .withMessage('First name should be string'),
    body('shipping')
      .optional()
      .custom((add) => Address.isAddress(add))
      .withMessage('Shipping must be complete.'),
    body('billing')
      .optional()
      .custom((add) => Address.isAddress(add))
      .withMessage('Billing must be complete.'),
  ],
  async (req: Request, res: Response) => {
    const errorsValidation = validationResult(req)

    if (!errorsValidation.isEmpty()) {
      return res.status(400).send({
        errors: [
          {
            message: 'Incomplete information',
            field: errorsValidation.array(),
          },
        ],
      })
    }

    const { userInfoTitle, firstName, lastName, shipping, billing } = req.body
    const { id } = req.params

    const userInfoExist = await UserInfo.findById(id)

    if (!userInfoExist) {
      return res.status(404).send({
        errors: [
          {
            message: 'User info not found',
          },
        ],
      })
    }

    if (userInfoTitle) userInfoExist.set({ userInfoTitle })
    if (firstName) userInfoExist.set({ firstName })
    if (lastName) userInfoExist.set({ lastName })

    if (shipping) {
      const shippingInfo = await AddressShippingInfo.findById(
        userInfoExist.shippingAddress
      )

      if (!shippingInfo) {
        const newShippingInfo = AddressShippingInfo.build({
          province: shipping.province,
          city: shipping.city,
          barangay: shipping.barangay,
          street: shipping.street,
        })

        await newShippingInfo.save()

        userInfoExist.shippingAddress = newShippingInfo
      } else {
        shippingInfo.set({
          province: shipping.province,
          city: shipping.city,
          barangay: shipping.barangay,
          street: shipping.street,
        })

        await shippingInfo.save()
      }
    }

    if (billing) {
      const billingInfo = await AddressBillingInfo.findById(
        userInfoExist.billingAddress
      )

      if (!billingInfo) {
        const newBillingInfo = await AddressBillingInfo.build({
          province: billing.province,
          city: billing.city,
          barangay: billing.barangay,
          street: billing.street,
        })

        await newBillingInfo.save()

        userInfoExist.billingAddress = newBillingInfo
      } else {
        billingInfo.set({
          province: billing.province,
          city: billing.city,
          barangay: billing.barangay,
          street: billing.street,
        })

        await billingInfo.save()
      }
    }

    await userInfoExist.save()

    await userInfoExist
      .populate('billingAddress')
      .populate('shippingAddress')
      .execPopulate()

    res.send(userInfoExist)
    // res.send('fng bug')
  }
)

export { router as addressUpdateRouter }
