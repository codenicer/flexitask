import express from 'express'
import { json } from 'body-parser'
import morgan from 'morgan'
import cors from 'cors'
import api from './api'
import { errorHandler } from './middleware/error_handler'
const app = express()

app.use(json())
app.use(morgan('tiny'))

app.use(cors())

app.use('/api', api)

app.use('*', (req, res) => {
  res.send({
    message: 'Hello there, i am from the back-end.',
  })
})

app.use(errorHandler)

export { app }
