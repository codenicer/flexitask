import { app } from './app'
import mongoose from 'mongoose'
;(async () => {
  const PORT = process.env.PORT || 3000

  await mongoose.connect('mongodb://flexiserver-mongo-srv/fleximongo', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })

  console.log('Connected to database')

  app.listen(PORT, () => {
    console.log(`Server started, listening to port ${PORT}`)
  })
})()
