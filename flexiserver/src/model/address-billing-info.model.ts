import mongoose, { Schema } from 'mongoose'
import { AddressAttrs, addressType } from '../utils/addresses'

export interface AddressBillingInfoDoc extends mongoose.Document {
  province?: string
  city?: string
  barangay?: string
  street?: string
  addressType: addressType.billing
}

interface AddressBillingInfoModel
  extends mongoose.Model<AddressBillingInfoDoc> {
  build(attrs: AddressAttrs): AddressBillingInfoDoc
}

const addressBillingSchema = new mongoose.Schema({
  province: {
    type: String,
  },
  city: {
    type: String,
  },
  barangay: {
    type: String,
  },
  street: {
    type: String,
  },
  addressType: {
    type: String,
    default: addressType.billing,
  },
})

addressBillingSchema.statics.build = (attrs: AddressAttrs) => {
  return new AddressBillingInfo(attrs)
}

const AddressBillingInfo = mongoose.model<
  AddressBillingInfoDoc,
  AddressBillingInfoModel
>('AddressBillingInfo', addressBillingSchema)

export { AddressBillingInfo }
