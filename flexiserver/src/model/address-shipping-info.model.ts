import mongoose from 'mongoose'
import { AddressAttrs, addressType } from '../utils/addresses'

export interface AddressShippingInfoDoc extends mongoose.Document {
  province: string
  city: string
  barangay: string
  street: string
  addressType: addressType.shipping
}

interface AddressShippingInfoModel
  extends mongoose.Model<AddressShippingInfoDoc> {
  build(attrs: AddressAttrs): AddressShippingInfoDoc
}

const addressShippingSchema = new mongoose.Schema({
  province: {
    type: String,
  },
  city: {
    type: String,
  },
  barangay: {
    type: String,
  },
  street: {
    type: String,
  },
  addressType: {
    type: String,
    default: addressType.shipping,
  },
})

addressShippingSchema.statics.build = (attrs: AddressAttrs) => {
  return new AddressShippingInfo(attrs)
}

const AddressShippingInfo = mongoose.model<
  AddressShippingInfoDoc,
  AddressShippingInfoModel
>('AddressShippingInfo', addressShippingSchema)

export { AddressShippingInfo }
