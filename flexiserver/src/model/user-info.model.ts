import mongoose from 'mongoose'
import { AddressShippingInfoDoc } from './address-shipping-info.model'
import { AddressBillingInfoDoc } from './address-billing-info.model'

interface UserInfoAttrs {
  userInfoTitle: string
  firstName?: String
  lastName?: String
  billingAddress?: AddressBillingInfoDoc
  shippingAddress?: AddressShippingInfoDoc
}
interface UserInfoDoc extends mongoose.Document {
  userInfoTitle: string
  firstName: String
  lastName: String
  billingAddress: AddressBillingInfoDoc
  shippingAddress: AddressShippingInfoDoc
}

interface UserInfoModel extends mongoose.Model<UserInfoDoc> {
  build(attrs: UserInfoAttrs): UserInfoDoc
}

const userInfoSchema = new mongoose.Schema({
  userInfoTitle: {
    type: String,
    require: true,
  },
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  billingAddress: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'AddressBillingInfo',
  },
  shippingAddress: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'AddressShippingInfo',
  },
})

userInfoSchema.statics.build = (attrs: UserInfoAttrs) => {
  return new UserInfo(attrs)
}

const UserInfo = mongoose.model<UserInfoDoc, UserInfoModel>(
  'UserInfo',
  userInfoSchema
)

export { UserInfo }
