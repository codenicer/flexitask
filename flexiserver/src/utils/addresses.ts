export interface AddressAttrs {
  province?: string
  city?: string
  barangay?: string
  street?: string
}

export enum addressType {
  billing = 'BILLING',
  shipping = 'SHIPPING',
}

export class Address {
  static isAddress(attrs: AddressAttrs) {
    try {
      if (
        !attrs.province ||
        typeof attrs.province !== 'string' ||
        attrs.province === ''
      ) {
        return false
      }
      if (
        !attrs.city ||
        typeof attrs.province !== 'string' ||
        attrs.city === ''
      ) {
        return false
      }
      if (
        !attrs.barangay ||
        typeof attrs.province !== 'string' ||
        attrs.barangay === ''
      ) {
        console.log('attrs1', attrs)
        return false
      }
      if (
        !attrs.street ||
        typeof attrs.province !== 'string' ||
        attrs.street === ''
      ) {
        return false
      }
    } catch (err) {
      console.log(err.message)
      return false
    }
    return true
  }
}
